﻿#include <iostream>
#include <string>
using namespace std;

struct Player
{
	string name;
	string age;
};

void name(string &name)
{
	cout << "Please, enter your name:";
	getline(cin, name);
}

void age(string &age)
{
	cout << "Please, enter your age:";
	getline(cin, age);
}

Player getinfo()
{
	string a;
	string b;
	name(a);
	age(b);
	Player one{ a,b };
	return one;
}

string outputinfo()
{
	Player playerOne = getinfo();
	string playerName = playerOne.name;
	string playerAge = playerOne.age;
	cout << "Your name is " << playerName << "\n";
	cout << "Your age is " << playerAge << "\n";
	cout << "Length of your name is " << playerName.length() << "\n";
	cout << "First letter of name is " << playerName.front() << "\n";
	cout << "Last letter is " << playerName.back() << "\n";
	cout << "First letter of your age is " << playerAge[0] << "\n";
	cout << "Last letter is " << playerAge[playerAge.length() - 1] << "\n";
	return playerName, playerAge;
}

int main()
{
	outputinfo();
	return 0;
}	
